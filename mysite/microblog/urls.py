from django.conf.urls import patterns, url

from microblog import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^index/$', views.index, name='index'),
    url(r'^login/$', views.log_in, name='log_in'),
    url(r'^logout/$', views.log_out, name='log_out'),
    url(r'^register/$', views.register, name='register'),
    url(r'^add/$', views.add, name='add'),
    url(r'^added/$', views.added, name='added'),
    url(r'^users/(?P<id>\d+)/$', views.users, name='users'),
    url(r'^selec/$', views.selec, name='selec'),
    url(r'^my/$', views.useres, name='my'),
    url(r'^sign/$', views.sign, name='sign'),
    url(r'^newpass', views.newpass, name='newpass')
)

urlpatterns += patterns('', (
    r'^static/(?P<path>.*)$',
    'django.views.static.serve',
    {'document_root': 'static'}
))
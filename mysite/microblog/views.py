# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.contrib import messages
from models import Entry
from django.shortcuts import render_to_response
from django.core.mail import send_mail
import datetime

entries = Entry.objects.all().order_by('-pk')
Users = User.objects.all()

def index(request):
    user = request.user
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users})

def log_in(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        if request.POST.get('go') == 'Login':
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return render(request, 'microblog/index.html', {'user':user, 'entries': entries, 'mess':'Poprawnie zalogowano!', 'users': Users})
            else:
                return render(request, 'microblog/login.html', {'message':'Invalid username or password!', 'users': Users})
        else:
            uses = User.objects.get(username__exact=username)
            if uses.count() == 1:
                newrandompassword = User.objects.make_random_password(length=10, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789')
                uses.set_password(newrandompassword)
                uses.save()
                send_mail('New Password to My Site - Blog', 'Hi, {{ uses.username }}\n\nHere is your new password to our site:\n{{ newrandompassword }}\n\nWe wait for you in 194.29.175.240/~p6/mysite/blog/login\nTeam Blog', 'blog@mysite.com', ['{{ uses.email }}'], fail_silently=False)
                messa = 'New Password in your mail !'
            else:
                messa = 'Invalid username !'
            return render(request,'microblog/login.html', {'message':messa, 'users': Users})
    else:
        return render(request, 'microblog/login.html', {'users': Users})

def log_out(request):
    logout(request)
    return render(request, 'microblog/index.html', {'entries': entries, 'mess':'Poprawnie wylogowano!', 'users': Users} )

def register(request):
    #entries = Entry.objects.all().order_by('-pk')
    #if request.method == "POST":
    #    username = request.POST.get('username')
    #    password = request.POST.get('password')
    #    confirmpassword = request.POST.get('confirmpassword')
    #    if password == confirmpassword:
    #        user = User.objects.create_user(username, None, password)
    #        user.is_staff = True
    #        user.full_clean()
    #        user.save()
    #        return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'mess':'Poprawnie zarejestrowano! Zaloguj sie', 'users': Users})
    #    else:
    #        return render(request, 'microblog/register.html', {'message':'Password is not confirmed!', 'users': Users})
    #else:
    return render(request, 'microblog/register.html', {'users': Users})

def add(request):
    if request.user.is_authenticated():
        user = request.user
        return render(request, 'microblog/add.html', {'user':user, 'users': Users})
    else:
        return render(request, 'microblog/login.html', {'users': Users})

def added(request):
    if request.method == "POST":
        text = request.POST.get('text')
        entry = Entry()
        entry.author = request.user
        entry.pub_date = datetime.datetime.now()
        entry.text = text
        entry.save()
        return render(request, 'microblog/index.html', {'user': request.user, 'entries': entries, 'mess': 'dodano wpis', 'users': Users})
    else:
        return render(request, 'microblog/add.html', {'users': Users})

def users(request, id):
    if id != 0:
        user = User.objects.filter(id=id)
        entries = Entry.objects.filter(author=user).order_by('-pk')
    else:
        entries = Entry.objects.all().order_by('-pk')
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users})

def useres(request):
    user = request.user
    entries = Entry.objects.filter(author=user).order_by('-pk')
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users})

def sign(request):
    if request.method == "POST":
        password = request.POST.get('password')
        password2 = request.POST.get('confirmpassword')
        if password == password2:
            try:
                username = request.POST.get('username')
                mail = request.POST.get('mail')
                user = User.objects.create_user(username, mail, password)
                user.is_staff = True
                user.save()
                mess = 'Konto utworzone.'
            except:
                return render(request, 'microblog/register.html', {'mess':'Login juz zajety !', 'users': Users})
            return render(request, 'microblog/login.html', {'message':'Konto utworzone !', 'users': Users})
        else:
            return render(request, 'microblog/register.html', {'mess':'Przepisz prawidlowo haslo !', 'users': Users})
    else:
        return render(request, 'microblog/register.html', {'mess':'Blad przy polczeniu, sprobuj jeszcze raz.', 'users': Users})

def newpass(request):
    if request.method == "POST":
        user = request.user
        old_password = request.POST.get('oldpassword')
        new_password = request.POST.get('newpassword')
        new_password2 = request.POST.get('confirmpassword')
        if user.check_password(old_password) and new_password == new_password2:
            u = User.objects.get(username__exact=user.username)
            u.set_password(new_password)
            u.save()
            logout(request)
            return render(request, 'microblog/login.html', {'mess':'Haslo zmieniono !', 'users': Users} )
        else:
            return render(request, 'microblog/newpassword.html', {'mess':'Bledne dane !', 'users':Users})
    else:
        return render(request, 'microblog/newpassword.html', {'users':Users})

def selec(request):
    liczbaa = request.POST.get('herolist')
    if liczbaa == 0:
        entries = Entry.objects.all().order_by('-pk')
    else:
        user = User.objects.filter(id=liczbaa)
        entries = Entry.objects.filter(author=user).order_by('-pk')
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users})
from django.db import models
from django.contrib.auth.models import User


class Entry(models.Model):
    author = models.ForeignKey(User)
    text = models.CharField(max_length=200)
    pub_date = models.DateTimeField()

    def __unicode__(self):
        return self.text